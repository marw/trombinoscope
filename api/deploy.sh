source api/function.sh

INDEX=$(mktemp /tmp/foo-XXXXX)

for student in content/*; do
    student_dir=$(echo $student/ | cut -d '/' -f 2 )
    make_url=$(echo $student_dir | tr '[:upper:]' '[:lower:]' | sed 's/áàâäçéèêëîïìôöóùúüñÂÀÄÇÉÈÊËÎÏÔÖÙÜÑ/aaaaceeeeiiiooouuunAAACEEEEIIOOUUN/g' | sed 's/ /-/g' )
    content_filepath=$student/README.md
    if [[ $( [ $(echo "$student_dir" | grep -E "[A-Za-z]+-[A-Za-z]+") ] && echo true ) ]]; then
        public_filepath="public/$make_url.html"
        echo "$content_filepath -> $public_filepath"
        eval $(parse_yaml $content_filepath)
        echo "- [$prenom $nom](./$make_url.html)" >> $INDEX

    pandoc \
        -s \
        -f markdown-auto_identifiers -t html5 \
        --template template/student.html \
        --metadata title="$prenom $nom" \
        --shift-heading-level-by=1 \
        -c style.min.css \
        $content_filepath \
        -o $public_filepath

    fi
done


pandoc \
        -s \
        -f markdown -t html5 \
        --template template/index.html \
        -c style.min.css \
        $INDEX \
        -o "public/index.html"

rm $INDEX
    